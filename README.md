# MPV-sync
Very small programm to sync multiple MPV instances.
## [Server](https://gitlab.com/TilCreator/websocket-broadcast-server)
## Concept
### participents
- multiple clients (one per instance)
> Connect to one room on the server and send and receive broadcast msgs. Msgs that are older (by timestamp) than the last local data are ignored.
- one server
> Blindly delivers broadcast msgs from clients to all other clients in one room.
### Schematic
```
               ╔════════════════╗
               ║     server     ║
               ║                ║
client_0 ──────╫─ room_0        ║
               ║  │             ║
client_1 ──────╫──┘             ║
               ║                ║
client_2 ──────╫─ room_1        ║
               ║  │             ║
client_3 ──────╫──┘             ║
               ╚════════════════╝
```
### Protocol v1.0
Undelying protocol is websocket. (So that a browser client is possible.)  
Rooms are selected by path, for example: `mpv-sync.example.com/hot-stuff` (`hot-stuff` is the room name)

base (`<>` are placeholders):
```json
{"timestamp": <unix-timestamp-utc>,
 "api_version": 1.0,
 "sender": <client_name>,
 "event": <event>,
 "data": <data>}
```
#### events
- "hello"
> Send by client to get all data from other clients on (re)connection.
```json
data = null
```
- "change"
> Send on a data change
```json
data = {"path": <path>,
        "play_state": <play_state>,
        "position": <position>,
        "speed": <speed>}
```
### Building
#### Linux (the easy way)
```
$ cargo build --release
```
#### Linux (the a bit harder way)
```
$ git clone https://github.com/mpv-player/mpv-build.git
$ MPV_SOURCE=mpv-build cargo build --release --features build_libmpv
```
#### Windows (crosscompile on Linux)
- Build https://github.com/shinchiro/mpv-winbuild-cmake  
- Move the build artifacs where libmpv-rs expects them
  ```
  $ ln -sr mpv-winbuild-cmake/build64/mpv-dev-x86_64-* mpv-winbuild-cmake/64
  ```
  If that doesn't work try this workaround
  ```
  $ mkdir mpv-winbuild-cmake/mpv
  $ ln -sr mpv-winbuild-cmake/build64/mpv-dev-x86_64-* mpv-winbuild-cmake/mpv/build
  ```
- Install the toolchain and build it
  ```
  $ rustup toolchain add stable-x86_64-pc-windows-gnu
  $ rustup target add x86_64-pc-windows-gnu
  $ MPV_SOURCE=mpv-winbuild-cmake cargo build --release --target x86_64-pc-windows-gnu --features build_libmpv
  ```