use async_trait::async_trait;
use failure::{format_err, Error};
use futures::stream::{Stream, StreamExt};
use serde_json::{json, Value};
use std::marker::Unpin;

use super::ntp_sync::get_timestamp;

pub mod websocket;

pub const API_VERSION: f64 = 1.0;

#[async_trait]
pub trait Communication: Stream<Item = Result<Message, Error>> + StreamExt + Unpin {
    async fn new() -> Result<Box<Self>, Error>;

    async fn open(&mut self, uri: &str) -> Result<(), Error>;
    async fn send(&mut self, message: Message) -> Result<(), Error>;
    async fn close(self) -> Result<(), Error>;
}

#[derive(Debug, Clone)]
pub struct Message {
    pub event: Event,
    pub timestamp: f64,
    pub sender: String,
    pub api_version: f64,
}

#[derive(Debug, Clone)]
pub enum Event {
    Hello,
    Change(SyncData),
}

#[derive(Debug, Clone)]
pub struct SyncData {
    pub path: String,
    pub play_state: bool,
    pub position: f64,
    pub speed: f64,
}

impl Message {
    pub fn new(event: Event, sender: &str, timestamp: Option<f64>) -> Result<Message, Error> {
        // timestamp = None will get the current timestamp
        Ok(Message {
            event,
            sender: sender.to_string(),
            api_version: API_VERSION,
            timestamp: timestamp.unwrap_or(get_timestamp()?),
        })
    }

    pub fn to_json(&self) -> Result<String, Error> {
        let event_data = match &self.event {
            Event::Hello => ("hello", Value::Null),
            Event::Change(data) => (
                "change",
                json!({
                    "path": data.path,
                    "play_state": data.play_state,
                    "position": data.position,
                    "speed": data.speed,
                }),
            ),
        };

        serde_json::to_string(&json!({
            "event": event_data.0,
            "data": event_data.1,
            "timestamp": self.timestamp,
            "api_version": self.api_version,
            "sender": self.sender,
        }))
        .map_err(|error| format_err!("serde_json::error: {}", error))
    }
}

pub fn parse_json_message(raw_string: &str) -> Result<Message, Error> {
    let raw_data: Value = serde_json::from_str(raw_string)?;

    if let Some(api_version_str) = raw_data.get("api_version") {
        if let Some(api_version) = api_version_str.as_f64() {
            if api_version != API_VERSION {
                warn!(
                    "Missmatching api version (own: {:?}, {}: {:?})",
                    API_VERSION,
                    raw_data
                        .get("sender")
                        .unwrap_or(&String::from("other").into())
                        .as_str()
                        .unwrap_or("other"),
                    api_version
                )
            }
        }
    }

    Ok(Message {
        event: match raw_data
            .get("event")
            .ok_or(format_err!(
                "serde_json::Error while parsing, event not found"
            ))?
            .as_str()
            .ok_or(format_err!(
                "serde_json::Error while parsing, event is not a string",
            ))? {
            "hello" => Event::Hello,
            "change" => {
                let data = raw_data.get("data").ok_or(format_err!(
                    "serde_json::Error while parsing, data not found"
                ))?;

                Event::Change(SyncData {
                    path: String::from(
                        data.get("path")
                            .ok_or(format_err!(
                                "serde_json::Error while parsing, path not found"
                            ))?
                            .as_str()
                            .ok_or(format_err!(
                                "serde_json::Error while parsing, path is not a string",
                            ))?,
                    ),
                    play_state: data
                        .get("play_state")
                        .ok_or(format_err!(
                            "serde_json::Error while parsing, play_state not found",
                        ))?
                        .as_bool()
                        .ok_or(format_err!(
                            "serde_json::Error while parsing, play_state is not a bool",
                        ))?,
                    position: data
                        .get("position")
                        .ok_or(format_err!(
                            "serde_json::Error while parsing, position not found",
                        ))?
                        .as_f64()
                        .ok_or(format_err!(
                            "serde_json::Error while parsing, position is not a f64",
                        ))?,
                    speed: data
                        .get("speed")
                        .ok_or(format_err!(
                            "serde_json::Error while parsing, speed not found"
                        ))?
                        .as_f64()
                        .ok_or(format_err!(
                            "serde_json::Error while parsing, speed is not a f64",
                        ))?,
                })
            }
            _ => Err(format_err!(
                "serde_json::Error while parsing, unknown event found",
            ))?,
        },
        timestamp: raw_data
            .get("timestamp")
            .ok_or(format_err!(
                "serde_json::Error while parsing, timestamp not found",
            ))?
            .as_f64()
            .ok_or(format_err!(
                "serde_json::Error while parsing, timestamp is not a f64",
            ))?,
        sender: String::from(
            raw_data
                .get("sender")
                .ok_or(format_err!(
                    "serde_json::Error while parsing, sender not found"
                ))?
                .as_str()
                .ok_or(format_err!(
                    "serde_json::Error while parsing, sender is not a string",
                ))?,
        ),
        api_version: raw_data
            .get("api_version")
            .ok_or(format_err!(
                "serde_json::Error while parsing, api_version not found",
            ))?
            .as_f64()
            .ok_or(format_err!(
                "serde_json::Error while parsing, api_version is not a f64",
            ))?,
    })
}
