use async_trait::async_trait;
use async_tungstenite::{
    tokio::{connect_async, ConnectStream},
    tungstenite::protocol::Message,
    WebSocketStream,
};
use failure::{format_err, Error};
use futures::{
    stream::Stream,
    task::{Context, Poll},
};
use futures::{SinkExt, StreamExt};
use log::{error, warn};
use std::{pin::Pin, result::Result};

use super::super::communication;

pub struct Communication {
    uri: Option<String>,
    ws_stream: Option<WebSocketStream<ConnectStream>>,
}

#[async_trait]
impl communication::Communication for Communication {
    async fn new() -> Result<Box<Self>, Error> {
        Ok(Box::new(Self {
            uri: None,
            ws_stream: None,
        }))
    }

    async fn open(&mut self, uri: &str) -> Result<(), Error> {
        assert!(self.ws_stream.is_none(), "Already open");

        self.uri = Some(uri.to_string());
        let (ws_stream, _) = connect_async(uri).await?;
        self.ws_stream = Some(ws_stream);

        Ok(())
    }

    async fn send(&mut self, message: communication::Message) -> Result<(), Error> {
        debug!("Sending message: {:?}", message);

        while self
            .ws_stream
            .as_mut()
            .unwrap()
            .send(Message::Text(message.to_json()?))
            .await
            .map_err(|error| format_err!("async_tungstenite::tungstenite::Error: {}", error))
            .map_err(|error| error!("{:?}", error))
            .is_err()
        {
            while {
                warn!("connection retry");
                self.ws_stream = None;
                let uri = self.uri.as_mut().unwrap().clone();
                self.open(&uri).await.is_err()
            } {}
        }

        Ok(())
    }

    async fn close(self) -> Result<(), Error> {
        self.ws_stream
            .unwrap()
            .close(None)
            .await
            .map_err(|error| format_err!("async_tungstenite::tungstenite::Error: {}", error))
    }
}

impl Stream for Communication {
    type Item = Result<communication::Message, Error>;

    fn poll_next(mut self: Pin<&mut Self>, context: &mut Context) -> Poll<Option<Self::Item>> {
        match self.ws_stream.as_mut().unwrap().poll_next_unpin(context) {
            Poll::Ready(Some(Ok(Message::Text(msg)))) => {
                debug!("Received text message: {:?}", msg);
                Poll::Ready(Some(communication::parse_json_message(&msg)))
            }
            Poll::Ready(Some(Ok(msg))) => {
                warn!("Received unhandled message: {:?}", msg);
                Poll::Pending // FIXME has the potential to sleep forever
            }
            Poll::Ready(Some(Err(e))) => {
                error!("while polling: {:?}", e);
                Poll::Pending // FIXME has the potential to sleep forever
            }
            Poll::Pending | Poll::Ready(None) => Poll::Pending,
        }
    }
}
