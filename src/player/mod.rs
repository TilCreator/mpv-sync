use async_trait::async_trait;
use failure::Error;
use futures::stream::{Stream, StreamExt};
use std::marker::Unpin;

use super::communication::SyncData;

pub mod mpv;

#[async_trait]
pub trait Player: Stream<Item = Result<Event, Error>> + StreamExt + Unpin {
    async fn new(player_options: &[String]) -> Result<Box<Self>, Error>;

    async fn update(&mut self, data: &SyncData) -> Result<(), Error>;

    async fn get_data(&self) -> Result<Event, Error>;

    async fn load_paths(&mut self, paths: &[&str]) -> Result<(), Error>;

    async fn show_message(&mut self, message: &str) -> Result<(), Error>;
}

#[derive(Debug, Clone)]
pub struct Event {
    pub event_type: EventType,
    pub timestamp: f64,
}

#[derive(Debug, Clone)]
pub enum EventType {
    None,
    Change(SyncData),
    Exit,
}

impl Event {
    pub fn is_none(&self) -> bool {
        matches!(self.event_type, EventType::None)
    }

    pub fn is_some(&self) -> bool {
        !self.is_none()
    }
}
