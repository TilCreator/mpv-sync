use async_trait::async_trait;
use failure::{format_err, Error};
use futures::{
    stream::Stream,
    task::{Context, Poll},
};
use libmpv::{events::Event, FileState, Format, Mpv};
use std::{
    pin::Pin,
    result::Result,
    sync::{Arc, Mutex},
    thread,
};

use super::super::communication::SyncData;
use super::super::ntp_sync::get_timestamp;
use super::super::player;

fn get_data_from_player(player: &Mpv) -> Result<player::Event, Error> {
    Ok(player::Event {
        timestamp: get_timestamp()?,
        event_type: player::EventType::Change(SyncData {
            path: match player.get_property("path") {
                Ok(path) => path,
                Err(libmpv::Error::Raw(-10)) => String::from(""),
                Err(error) => Err(format_err!("libmpv::Error while getting path: {}", error))?,
            },
            play_state: match player.get_property::<bool>("pause") {
                Ok(pause) => !pause,
                Err(error) => Err(format_err!(
                    "libmpv::Error while getting play_state: {}",
                    error
                ))?,
            },
            position: match player.get_property("time-pos") {
                Ok(position) => position,
                Err(libmpv::Error::Raw(-10)) => 0.0,
                Err(error) => Err(format_err!(
                    "libmpv::Error while getting position: {}",
                    error
                ))?,
            },
            speed: match player.get_property("speed") {
                Ok(speed) => speed,
                Err(error) => Err(format_err!("libmpv::Error while getting speed: {}", error))?,
            },
        }),
    })
}

pub struct Player {
    player: Arc<Mpv>,
    event_thread: Option<thread::JoinHandle<()>>,
    last_event: Arc<Mutex<player::Event>>,
    latest_messages: Vec<(f64, String)>,
}

#[async_trait]
impl player::Player for Player {
    async fn new(player_options: &[String]) -> Result<Box<Self>, Error> {
        let mpv = Arc::new(
            Mpv::with_initializer(|pre_mpv| {
                pre_mpv.set_property("config", "yes")?;
                pre_mpv.set_property("idle", "yes")?;
                pre_mpv.set_property("terminal", "no")?;
                pre_mpv.set_property("input-terminal", "no")?;
                pre_mpv.set_property("force-window", "yes")?;
                pre_mpv.set_property("osc", "yes")?;
                pre_mpv.set_property("osd-duration", 2000)?; // Displays messages a bit longer so one can actually read them
                pre_mpv.set_property("input-default-bindings", "yes")?;
                pre_mpv.set_property("input-vo-keyboard", "yes")?;
                pre_mpv.set_property("input-media-keys", "yes")?;
                pre_mpv.set_property("keep-open", "yes")?;

                player_options.iter().try_for_each(|player_option| {
                    const PLAYER_OPTION_ERROR: &str =
                        "Error while parsing player options, must be formated <property>=<value>";

                    let mut split = player_option.splitn(2, "=");
                    let (property, value) = (
                        split.next().expect(PLAYER_OPTION_ERROR),
                        split.next().expect(PLAYER_OPTION_ERROR),
                    );
                    debug!("Setting property {:?} to {:?}", property, value);
                    pre_mpv.set_property(property, value)
                })
            })
            .map_err(|error| format_err!("libmpv::Error while initializing mpv: {}", error))?,
        );

        Ok(Box::new(Self {
            player: mpv,
            event_thread: None,
            last_event: Arc::new(Mutex::new(player::Event {
                timestamp: 0.0, // None evens don't need a timestamp
                event_type: player::EventType::None,
            })),
            latest_messages: vec![],
        }))
    }

    async fn get_data(&self) -> Result<player::Event, Error> {
        get_data_from_player(&self.player)
    }

    async fn update(&mut self, data: &SyncData) -> Result<(), Error> {
        match self.get_data().await?.event_type {
            player::EventType::Change(state) => {
                if state.path != data.path {
                    self.player
                        .playlist_load_files(&[(&data.path, FileState::Replace, None)])
                        .map_err(|error| format_err!("libmpv::Error setting path: {}", error))?;

                    debug!("Set path to {:?}", data.path);
                }
                if state.play_state != data.play_state {
                    self.player
                        .set_property("pause", !data.play_state)
                        .map_err(|error| {
                            format_err!("libmpv::Error setting play_state: {}", error)
                        })?;

                    debug!("Set play_state to {:?}", data.play_state);
                }
                if state.position != data.position {
                    match self
                        .player
                        .set_property("time-pos", data.position)
                        .map_err(|error| format_err!("libmpv::Error setting position: {}", error))
                    {
                        Ok(_) => debug!("Set position to {:?}", data.position),
                        Err(_) => {
                            debug!("Setting position failed, trying start property");

                            self.player
                                .set_property("start", data.position.to_string())
                                .map_err(|error| {
                                    format_err!("libmpv::Error setting start position: {}", error)
                                })?;
                        }
                    }
                }
                if state.speed != data.speed {
                    self.player
                        .set_property("speed", data.speed)
                        .map_err(|error| format_err!("libmpv::Error setting speed: {}", error))?;

                    debug!("Set speed to {:?}", data.speed);
                }

                Ok(())
            }
            player::EventType::None | player::EventType::Exit => {
                Err(format_err!("Couldn't get player state"))
            }
        }
    }

    async fn load_paths(&mut self, paths: &[&str]) -> Result<(), Error> {
        paths.iter().try_for_each(|path| {
            self.player
                .playlist_load_files(&[(path, FileState::Append, None)])
                .map_err(|error| format_err!("libmpv::Error in set_paths: {}", error))
        })
    }

    async fn show_message(&mut self, message: &str) -> Result<(), Error> {
        self.latest_messages
            .push((get_timestamp()?, String::from(message)));

        let timeout_time = get_timestamp()?
            - self
                .player
                .get_property::<f64>("osd-duration")
                .map(|duration| duration / 1000.0)
                .unwrap_or(1.0);
        self.latest_messages = self
            .latest_messages
            .drain(..)
            .filter(|message| message.0 > timeout_time)
            .collect();

        self.player
            .command(
                "show-text",
                &[&format!(
                    "\"{}\"",
                    self.latest_messages
                        .iter()
                        .fold(String::from(""), |acc, message| format!(
                            "{}{}\n",
                            acc, message.1
                        ))
                        .replace("\"", "\\\"")
                )],
            )
            .map_err(|error| format_err!("libmpv::Error sending message to mpv: {}", error))
    }
}

impl Stream for Player {
    type Item = Result<player::Event, Error>;

    fn poll_next(mut self: Pin<&mut Self>, context: &mut Context) -> Poll<Option<Self::Item>> {
        if self.event_thread.is_none() {
            // Variables used in the thread
            let mpv = Arc::clone(&self.player);
            let mpv_last_event = Arc::clone(&self.last_event);
            let waker = Arc::new(context.waker().clone());

            self.event_thread = Some(thread::spawn(move || {
                let mut event_context = mpv.create_event_context();

                event_context.enable_all_events().unwrap();
                event_context
                    .observe_property("speed", Format::Double, 0)
                    .unwrap();
                event_context
                    .observe_property("pause", Format::Flag, 0)
                    .unwrap();
                event_context.disable_deprecated_events().unwrap();

                loop {
                    if mpv_last_event.lock().unwrap().is_none() {
                        if let Some(event) = event_context.wait_event(10.) {
                            let mut last_event = mpv_last_event.lock().unwrap();

                            if let Ok(Event::Shutdown) = event {
                                *last_event = player::Event {
                                    timestamp: get_timestamp().unwrap(),
                                    event_type: player::EventType::Exit,
                                }
                            } else {
                                let noteworthy_event: bool = match &event {
                                    Ok(event) => match event {
                                        Event::Shutdown => true,
                                        Event::StartFile => true,
                                        Event::PlaybackRestart => true,
                                        Event::PropertyChange { name: "speed", .. } => true,
                                        Event::PropertyChange { name: "pause", .. } => true,
                                        Event::FileLoaded => true,
                                        Event::Seek => true,
                                        // Ignored events
                                        Event::AudioReconfig => false,
                                        Event::VideoReconfig => false,
                                        Event::Deprecated(..) => false, // Ignore depreciated events
                                        Event::EndFile(..) => false,
                                        e => {
                                            warn!("Unhandeled mpv event: {:?}", e);
                                            false
                                        }
                                    },
                                    Err(e) => {
                                        match e {
                                            libmpv::Error::Raw(-13) => {
                                                warn!("Failed loading file");
                                            }
                                            _ => {
                                                warn!("{:?}", e);
                                            }
                                        }

                                        false
                                    }
                                };

                                *last_event = match noteworthy_event {
                                    false => player::Event {
                                        timestamp: 0.0,
                                        event_type: player::EventType::None,
                                    },
                                    true => get_data_from_player(&mpv).unwrap(),
                                };

                                if noteworthy_event {
                                    debug!("event = {:?}, data = {:?}", event, last_event);
                                }
                            }

                            if last_event.is_some() {
                                waker.wake_by_ref(); // last_event is filled again, so the stream is not pending anymore

                                if let player::EventType::Exit = last_event.event_type {
                                    // Player is dead, so no more events are expected
                                    break;
                                }
                            }
                        }
                    } else {
                        thread::park(); // last_event is still full, so the thread will wait until it's called by poll_next
                    }
                }
            }));
        }

        let last_event_clone = self.last_event.lock().unwrap().clone();

        if last_event_clone.is_some() {
            *self.last_event.lock().unwrap() = player::Event {
                timestamp: 0.0, // None evens don't need a timestamp
                event_type: player::EventType::None,
            };

            self.event_thread.as_ref().unwrap().thread().unpark(); // last_event is empty, so the thread can catch the next one

            Poll::Ready(Some(Ok(last_event_clone)))
        } else {
            Poll::Pending // Wait until there are further events to process
        }
    }
}
