use failure::{format_err, Error};
use sntpc;
use std::{
    net::UdpSocket,
    sync::{Arc, Mutex},
    thread, time,
    time::{Duration, SystemTime, UNIX_EPOCH},
};

lazy_static! {
    static ref OFFSET: Arc<Mutex<Option<f64>>> = Arc::new(Mutex::new(None));
}

pub fn init_with_offset(offset: f64) {
    let mut offset_option = OFFSET.lock().unwrap();
    *offset_option = Some(offset);
}

pub fn init(count_min: i64, server: String) -> Result<(), Error> {
    let mut count = 0;

    let mut offset_option = OFFSET.lock().unwrap();
    *offset_option = Some(0.0);
    let offset = offset_option.as_mut().unwrap();

    let socket = UdpSocket::bind("0.0.0.0:0")?;
    socket.set_read_timeout(Some(Duration::from_secs(2)))?;

    while count < count_min {
        *offset = 1.0_f64 / (count + 1) as f64
            * ((*offset * count as f64)
                + loop {
                    let result = sntpc::simple_get_time(&format!("{}:{}", server, 123), &socket);

                    match result {
                        Ok(time) => {
                            assert_ne!(time.sec(), 0);
                            let ntp_time: f64 = format!("{}.{}", time.sec(), time.sec_fraction())
                                .parse::<f64>()
                                .unwrap();

                            let system_time = SystemTime::now()
                                .duration_since(UNIX_EPOCH)
                                .expect("Time went backwards")
                                .as_secs_f64();

                            break ntp_time - system_time;
                        }
                        Err(err) => warn!("Err: {:?}", err),
                    }

                    thread::sleep(time::Duration::from_secs_f64(1.0));
                });

        debug!(
            "NTP sync: count: {:?}/{:?}, offset: {:?}",
            count + 1,
            count_min,
            offset
        );

        count += 1;

        thread::sleep(time::Duration::from_secs_f64(0.1));
    }
    Ok(())
}

pub fn get_offset() -> Result<f64, Error> {
    OFFSET
        .lock()
        .unwrap()
        .ok_or(format_err!("offset is not set"))
}

pub fn get_timestamp() -> Result<f64, Error> {
    Ok(SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards")
        .as_secs_f64()
        + OFFSET
            .lock()
            .unwrap()
            .ok_or(format_err!("offset is not set"))?)
}
