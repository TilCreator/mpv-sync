#![recursion_limit = "2048"]

mod communication;
pub mod ntp_sync;
mod player;

use crate::communication::Communication;
use crate::player::Player;
use clap::{App, Arg};
use failure::{bail, format_err, Error};
use futures::StreamExt;
use std::{
    env::{temp_dir, var},
    sync::Arc,
    time::Duration,
};
use tokio::{
    select,
    sync::{
        mpsc::{channel, Sender},
        RwLock,
    },
    time::timeout,
};
#[macro_use]
extern crate log;
#[macro_use]
extern crate serde;
#[macro_use]
extern crate lazy_static;

#[derive(Debug, Clone)]
struct Timestamped<D> {
    timestamp: f64,
    data: D,
}

#[derive(Debug, Serialize, Deserialize)]
struct Config {
    verbose: bool,
    server: String,
    player_options: Vec<String>,
    room: String,
    name: String,
    die_after_path: bool,
    ntp_server: String,
    ntp_sync_count: i64,
    ntp_force_sync: bool,
    init_timeout: f64,
    position_max_deviation: f64,
    puppet_only: bool,
    event_timeout: f64,
    tmp_file: String,
    hide_messages_in_player: bool,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            verbose: false,
            server: String::from("wss://websocket-relay.tils.pw"),
            player_options: vec![],
            room: String::from(""),
            name: String::from(""),
            die_after_path: false,
            ntp_server: String::from("pool.ntp.org"),
            ntp_sync_count: 10,
            ntp_force_sync: false,
            init_timeout: 2.0,
            position_max_deviation: 0.5,
            puppet_only: false,
            event_timeout: 1.0,
            tmp_file: {
                let mut path = temp_dir();
                path.push("mpv-sync_tmp.toml");
                path.into_os_string().into_string().unwrap()
            },
            hide_messages_in_player: false,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct TmpData {
    ntp_offset: f64,
    ntp_accuracy: i64,
    ntp_server: String,
}

impl Default for TmpData {
    fn default() -> Self {
        Self {
            ntp_offset: 0.0,
            ntp_accuracy: -1,
            ntp_server: String::from(""),
        }
    }
}

// TODO This quickly became a mess, the player message part should be redone at least partialy
lazy_static! {
    static ref PLAYER_MESSAGE_SENDER: Arc<RwLock<Option<Sender<String>>>> =
        Arc::new(RwLock::new(None));
}

async fn player_show_message(message: String) -> Result<Result<(), Error>, Error> {
    match PLAYER_MESSAGE_SENDER.write().await {
        player_message_sender if player_message_sender.is_some() => player_message_sender
            .clone()
            .unwrap()
            .send(message)
            .await
            .map(Ok)
            .map_err(|error| format_err!("tokio::sync::mpsc::error::SendError: {}", error)),
        _ => Ok(Err(format_err!("player message channel is not ready"))),
    }
}

macro_rules! info { // TODO This macro currently only works in an async context
    (target: $target:expr, $($arg:tt)+) => (
        log::info!(target: $target, $($arg)+);

        // Wrap in `Runtime::new().unwrap().block_on()` if not in async, but idk how to detect that
        if let Err(error) = player_show_message(format!($($arg)+)).await.unwrap() {
            debug!("{}", error);
        }
    );
    ($($arg:tt)+) => (
        log::info!($($arg)+);

        // Wrap in `Runtime::new().unwrap().block_on()` if not in async, but idk how to detect that
        if let Err(error) = player_show_message(format!($($arg)+)).await.unwrap() {
            debug!("{}", error);
        }
    )
}

async fn oracle(
    state: &Timestamped<communication::SyncData>,
    play_state_change: bool,
    now: Option<f64>,
) -> Result<communication::SyncData, Error> {
    // All times in seconds
    // Function to "predict" where another client seeked to/paused at given the time and the action of the event
    let now = now.unwrap_or(ntp_sync::get_timestamp()?);

    let offset = (now - state.timestamp) * state.data.speed;

    let mut player_state = state.data.clone();

    /* | play_state | change | action
    /  | 0          | 0      | position
    /  | 0          | 1      | position - offset
    /  | 1          | 0      | position + offset
    /  | 1          | 1      | position + offset */

    player_state.position += if !player_state.play_state && !play_state_change {
        0.0
    } else if !player_state.play_state && play_state_change {
        -offset
    } else {
        offset
    };

    Ok(player_state)
}

async fn change_is_relevant(
    data: &Timestamped<communication::SyncData>,
    change: &communication::SyncData,
    config: &Config,
    user: Option<&str>, // Set the Some(..) to print the change to stdout
) -> Result<bool, Error> {
    let predicted_position = oracle(data, data.data.play_state != change.play_state, None)
        .await?
        .position;

    let path_change = change.path != data.data.path;
    let play_state_change = change.play_state != data.data.play_state;
    let speed_change = change.speed != data.data.speed;
    let position_change = if change.play_state {
        change.position > predicted_position + config.position_max_deviation
            || change.position < predicted_position - config.position_max_deviation
    } else {
        // Don't ignore slight skips when paused
        change.position > predicted_position + 0.01 || change.position < predicted_position - 0.01
    };

    if let Some(user) = user {
        let user_part = if user == config.name {
            String::from("You")
        } else {
            format!("User {}", user)
        };

        if path_change {
            info!("{user} opened {path}", user = user_part, path = change.path);
        }
        if play_state_change {
            info!(
                "{user} {play_state}",
                user = user_part,
                play_state = if change.play_state {
                    "started playing"
                } else {
                    "paused"
                }
            );
        }
        if speed_change {
            info!(
                "{user} changed speed to {speed}",
                user = user_part,
                speed = change.speed
            );
        }
        if position_change {
            debug!("{} {}", change.position, data.data.position);
            info!(
                "{user} seeked to {position}",
                user = user_part,
                position = change.position
            );
        }
    }

    Ok(path_change || play_state_change || speed_change || position_change)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let args = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .about(&*format!(
            "Very small programm to sync multiple MPV instances\nAPI Version: {:?}\n\nMany options can be overwriten by the config file, so check this when expirencing unexpected behaviour.",
            communication::API_VERSION
        ))
        .arg(
            Arg::with_name("path")
                .multiple(true)
                .help("The path(s) to play, has to be left blank to follow other clients"),
        )
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .long("verbose")
                .help("Enables verbose output, you can also use RUST_LOG env var to set the log level."),
        )
        .arg(
            Arg::with_name("server")
                .short("s")
                .long("server")
                .value_name("(ws|wss)://address[:<port>]")
                .help("To which server to connect to"),
        )
        .arg(
            Arg::with_name("player-options")
                .short("o")
                .long("player-options")
                .value_name("property>=<value")
                .multiple(true)
                .help("Extra options given to the player\nExample for MPV: -o fs=yes"),
        )
        .arg(
            Arg::with_name("room")
                .short("r")
                .long("room")
                .takes_value(true)
                .help("The room to connect to"),
        )
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .takes_value(true)
                .help("Path to config file to load (instead of default one)"),
        )
        .arg(
            Arg::with_name("name")
                .short("n")
                .long("name")
                .takes_value(true)
                .help("Name to use when sending messages\nUses user name when empty"),
        )
        .arg(
            Arg::with_name("die-after-path")
                .short("d")
                .long("die-after-path")
                .help("Don't start player and don't persist after setting the path in a room\nUseful for scripting"),
        )
        .arg(
            Arg::with_name("ntp-server")
                .long("ntp-server")
                .takes_value(true)
                .help("NTP Server to use for getting a more exact timestamp"),
        )
        .arg(
            Arg::with_name("ntp-sync-count")
                .long("ntp-sync-count")
                .takes_value(true)
                .help("How many times the offset of the local timestamp and the ntp server are messured before beeing averaged out"),
        )
        .arg(
            Arg::with_name("ntp-force-sync")
                .short("f")
                .long("ntp-force-sync")
                .help("Forces a ntp sync even if there is a cached one already."),
        )
        .arg(
            Arg::with_name("init-timeout")
                .short("t")
                .long("init-timeout")
                .takes_value(true)
                .help("How long it waits until giving up on finding other clients in a room"),
        )
        .arg(
            Arg::with_name("position-max-deviation")
                .short("m")
                .long("position-max-deviation")
                .takes_value(true)
                .help("How far the position has to change before a change is recogniced"),
        )
        .arg(
            Arg::with_name("puppet-only")
                .short("p")
                .long("puppet-only")
                .help("Will never send status updates, just receive them"),
        )
        .arg(
            Arg::with_name("event-timeout")
                .long("event-timeout")
                .takes_value(true)
                .help("How long the client should wait at max before checking what the player is actually doing"),
        )
        .arg(
            Arg::with_name("tmp-file")
                .long("tmp-file")
                .takes_value(true)
                .help("File to save temporary data, should be in a location that is lost after a reboot"),
        )
        .arg(
            Arg::with_name("generate-config")
                .short("g")
                .long("generate-config")
                .help("Generates a config from the current config and command line options, is written to stdout. Works with --config"),
        )
        .arg(
            Arg::with_name("hide-messages-in-player")
                .short("i")
                .long("hide-messages-in-player")
                .help("Hides the messages in the player"),
        )
        .get_matches();

    let config_file: Config = if let Some(config_path) = args.value_of("config") {
        confy::load_path(config_path)
    } else {
        confy::load("mpv-sync", Some("mpv-sync"))
    }?;

    let paths: Vec<&str> = if let Some(values) = args.values_of("path") {
        values.collect()
    } else {
        vec![]
    };

    let config: Config = Config {
        verbose: args.is_present("verbose") || config_file.verbose,
        server: args
            .value_of("server")
            .map(String::from)
            .unwrap_or(config_file.server.clone()),
        player_options: {
            // Merge player-options and keep config_file options in front
            let mut player_options: Vec<String> = config_file.player_options.clone();
            if let Some(values) = args.values_of("player-options") {
                player_options.extend(
                    values
                        .map(|player_option: &str| String::from(player_option))
                        .collect::<Vec<String>>(),
                );
            }
            player_options
        },
        room: args
            .value_of("room")
            .map(String::from)
            .unwrap_or(config_file.room.clone()),
        name: {
            let mut name = args
                .value_of("name")
                .map(String::from)
                .unwrap_or(config_file.name.clone());
            if name.is_empty() {
                name = whoami::username();
            }
            name
        },
        die_after_path: args.is_present("die-after-path") || config_file.die_after_path,
        ntp_server: args
            .value_of("ntp-server")
            .map(String::from)
            .unwrap_or(config_file.ntp_server.clone()),
        ntp_sync_count: args
            .value_of("ntp-sync-count")
            .map(|ntp_sync_count| ntp_sync_count.parse::<i64>())
            .unwrap_or(Ok(config_file.ntp_sync_count))?,
        ntp_force_sync: args.is_present("ntp-force-sync") || config_file.ntp_force_sync,
        init_timeout: args
            .value_of("init-timeout")
            .map(|init_timeout| init_timeout.parse::<f64>())
            .unwrap_or(Ok(config_file.init_timeout))?,
        position_max_deviation: args
            .value_of("position-max-deviation")
            .map(|position_max_deviation| position_max_deviation.parse::<f64>())
            .unwrap_or(Ok(config_file.position_max_deviation))?,
        puppet_only: args.is_present("puppet-only") || config_file.puppet_only,
        event_timeout: args
            .value_of("event-timeout")
            .map(|event_timeout| event_timeout.parse::<f64>())
            .unwrap_or(Ok(config_file.event_timeout))?,
        tmp_file: args
            .value_of("tmp-file")
            .map(String::from)
            .unwrap_or(config_file.tmp_file.clone()),
        hide_messages_in_player: args.is_present("hide-messages-in-player")
            || config_file.hide_messages_in_player,
    };

    let mut tmp_data: TmpData = confy::load_path(&config.tmp_file)?;

    let mut data = Timestamped {
        timestamp: 0.0,
        data: communication::SyncData {
            path: String::from(""),
            play_state: false,
            position: 0.0,
            speed: 1.0,
        },
    };

    pretty_env_logger::formatted_timed_builder()
        .parse_filters(&match var("RUST_LOG") {
            Ok(val) => val,
            Err(_) => {
                if config.verbose {
                    String::from("debug")
                } else {
                    String::from("info")
                }
            }
        })
        .init();

    debug!("args = {:#?}", args);
    debug!(
        "config_path = {}",
        args.value_of("config").unwrap_or("<default>")
    );
    debug!("config_file = {:#?}", config_file);
    debug!("tmp_data = {:#?}", tmp_data);
    debug!("config = {:#?}", config);
    debug!("paths = {:#?}", paths);
    debug!("data = {:#?}", data);

    if args.is_present("generate-config") {
        println!("{}", toml::to_string(&config)?);

        return Ok(());
    }

    if config.die_after_path && paths.is_empty() {
        bail!("Can not die-after-path without a path");
    }

    // Creates a channel for messages which will be displayed by the player
    let mut player_message_receiver = {
        let (sender, receiver) = channel(10);

        let mut player_message_sender = PLAYER_MESSAGE_SENDER.write().await;
        *player_message_sender = Some(sender);

        receiver
    };

    if config.ntp_force_sync
        || tmp_data.ntp_server != config.ntp_server
        || tmp_data.ntp_accuracy < config.ntp_sync_count
    {
        info!("Starting ntp time sync");

        ntp_sync::init(config.ntp_sync_count, config.ntp_server.clone())?;

        tmp_data.ntp_accuracy = config.ntp_sync_count;
        tmp_data.ntp_server = config.ntp_server.clone();
        tmp_data.ntp_offset = ntp_sync::get_offset()?;

        debug!("Saved tmp_data = {:#?}", tmp_data);

        confy::store_path(&config.tmp_file, &tmp_data)?;
    } else {
        debug!("Using cached ntp offset");

        ntp_sync::init_with_offset(tmp_data.ntp_offset);
    }

    info!("You'r systems time seams to be {:?}sec off compared to {} (this will be compensated for automaticaly)", tmp_data.ntp_offset, tmp_data.ntp_server);

    let mut communication = communication::websocket::Communication::new().await?;
    communication
        .open(&format!("{}/{}", &config.server, &config.room))
        .await?;
    info!(
        "Joined room {:?} on {:?} as {:?}",
        config.room, config.server, config.name
    );

    debug!("Saying hello");
    communication
        .send(communication::Message::new(
            communication::Event::Hello,
            &config.name,
            None,
        )?)
        .await?;

    let known_users: Arc<RwLock<Vec<String>>> = Arc::new(RwLock::new(vec![]));
    loop {
        let communication = &mut communication;
        let known_users_ = known_users.clone();

        match timeout(Duration::from_secs_f64(config.init_timeout), async move {
            loop {
                match communication.next().await {
                    Some(Ok(msg)) => {
                        if !known_users_.read().await.contains(&msg.sender) {
                            break msg.clone();
                        }
                    }
                    Some(Err(error)) => {
                        warn!("Error receiving message: {}", error);
                        continue;
                    }
                    None => continue,
                }
            }
        })
        .await
        {
            Ok(message) => {
                if message.sender != config.name {
                    if let communication::Event::Change(change) = message.event {
                        info!("{:?} found", message.sender);

                        data.timestamp = message.timestamp;
                        data.data = change;

                        known_users.write().await.push(message.sender);
                    }
                }
            }
            Err(_) => {
                // timeout
                debug!("Timeout finding other clients");
                break;
            }
        }
    }

    debug!("Data after finding other clients {:#?}", data);

    if data.timestamp == 0.0 {
        data.timestamp = ntp_sync::get_timestamp()?;
    }

    if !paths.is_empty() {
        data.timestamp = ntp_sync::get_timestamp()?;
        data.data.path = paths.first().unwrap().to_string();
        data.data.position = 0f64;

        communication
            .send(communication::Message::new(
                communication::Event::Change(data.data.clone()),
                &config.name,
                None,
            )?)
            .await?;
    }

    debug!("Data after init {:#?}", data);

    if config.die_after_path {
        return Ok(());
    }

    let mut player = player::mpv::Player::new(&config.player_options).await?;

    player.update(&oracle(&data, false, None).await?).await?;
    if !paths.is_empty() {
        player.load_paths(&paths[1..]).await?;
    }

    // Empty player events queue
    while timeout(Duration::from_secs_f64(0.1), player.next())
        .await
        .is_ok()
    {}

    loop {
        select! {
            event = timeout(
                Duration::from_secs_f64(config.event_timeout), // Check every X secs for possible changes
                player.next(),
            ) => {
                match
                    match event {
                        Ok(event) => event,
                        Err(_) => Some(player.get_data().await),
                    }
                {
                    Some(Ok(event)) => {
                        debug!("Received event: {:?}", event);

                        match event.event_type {
                            player::EventType::Change(change) => {
                                if change_is_relevant(&data, &change, &config, Some(&config.name)).await? { // Ignore slight changes
                                    if event.timestamp > data.timestamp && !config.puppet_only {
                                        communication
                                            .send(
                                                communication::Message::new(
                                                    communication::Event::Change(change.clone()),
                                                    &config.name,
                                                    Some(event.timestamp)
                                                )?
                                            )
                                            .await?;

                                        data.timestamp = event.timestamp;
                                        data.data = change.clone();
                                    } else {
                                        player.update(
                                            &oracle(&data, data.data.play_state != change.play_state, None).await?
                                        )
                                        .await?;
                                    }
                                }
                            },
                            player::EventType::Exit => {
                                warn!("Player closed");
                                break;
                            },
                            player::EventType::None => (),
                        }
                    }
                    Some(Err(error)) => warn!("Error receiving event: {}", error),
                    None => (), // No new event
                }
            },
            message = communication.next() => {
                match message {
                    Some(Ok(message)) => {
                        if message.sender != config.name {
                            debug!("Received message: {:?}", message);

                            match message.event {
                                communication::Event::Change(change) => {
                                    if message.timestamp > data.timestamp {
                                        let play_state_change =
                                            change.play_state != data.data.play_state;

                                        if change_is_relevant(&data, &change, &config, Some(&message.sender)).await? { // Ignore slight changes
                                            data.timestamp = message.timestamp;
                                            data.data = change.clone();

                                            {
                                                let oracle_result = oracle(&data, play_state_change, None).await?;
                                                debug!("New data: {:?}, (oracle: {:?})", &data, &oracle_result.position);

                                                player.update(
                                                    &oracle_result
                                                )
                                                .await?;
                                            }
                                        }
                                    } else {
                                        warn!("Ignoring message, because it's too old");
                                    }
                                },
                                communication::Event::Hello => {
                                    info!("{:?} said hello", message.sender);

                                    communication
                                        .send(
                                            communication::Message::new(
                                                communication::Event::Change(data.data.clone()),
                                                &config.name,
                                                Some(data.timestamp)
                                            )?
                                        )
                                        .await?;
                                },
                            }
                        }
                    }
                    Some(Err(error)) => warn!("Error receiving message: {}", error),
                    None => (), // No new message
                }
            },
            Some(message) = player_message_receiver.recv() => {
                if !config.hide_messages_in_player {
                    if let Err(error) = player.show_message(&message).await {
                        warn!("Failed showing message on player: {}", error);
                    }
                }
            }
        }
    }

    communication.close().await?;

    Ok(())
}
